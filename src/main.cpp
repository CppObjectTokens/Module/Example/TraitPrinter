/*
 * Copyright (c) 2018-2020 Viktor Kireev
 * Distributed under the MIT License
 */

#include <otn/all.hpp>

#include <iostream>

#ifdef __cpp_concepts
template <class Token>
requires(otn::is_owner_v<Token>)
#else
template <class Token, OTN_CONCEPT_REQUIRES(otn::is_owner_v<Token>)>
#endif
void printToken(const Token& owner)
{
    using namespace std;

    if constexpr (otn::is_optional_v<Token>)
    {
        if (owner)
            cout << "owner optional token value = " << *owner << endl;
        else
            cout << "owner optional token is empty" << endl;
    }
    else if constexpr (otn::is_single_v<Token>)
    {
        cout << "owner single token value = " << *owner << endl;
    }
}

#ifdef __cpp_concepts
template <class Token>
requires(otn::is_observer_v<Token>)
#else
template <class Token, OTN_CONCEPT_REQUIRES(otn::is_observer_v<Token>)>
#endif
void printToken(const Token& observer)
{
    using namespace std;

    if (auto loc = otn::gain(observer))
        cout << "observer token value = " << *loc << endl;
    else
        cout << "observer token is empty" << endl;
}

#ifdef __cpp_concepts
template <class Token>
requires(otn::is_token_v<Token>)
#else
template <class Token, OTN_CONCEPT_REQUIRES(otn::is_token_v<Token>)>
#endif
void printAnyToken(const Token& any_token)
{
    using namespace std;

    cout << "any printer:\n    ";

    printToken(any_token);
}

template <class Token,
          class = std::enable_if_t<otn::element_is_v<Token, std::string>>>
void printTokenByType(const Token& string_token)
{
    using namespace std;

    cout << "std::string printer:\n    ";
    printToken(string_token);
}

template <class Token>
std::enable_if_t<otn::element_is_v<Token, double>>
printTokenByType(const Token& double_token)
{
    using namespace std;

    cout << "double printer:\n    ";
    printToken(double_token);
}

int main()
{
    using namespace std;

    {
        using namespace otn;

        cout << "otn tokens:" << endl;

        unique_optional<string>    unique_string{itself, "Hello, World!"};
        weak_optional<string>      weak_string{unique_string};
        raw::weak_optional<string> observer_string{unique_string};
        shared_single<double>      shared_double{itself, 42.43};
        unified<double> unified_double{shared_double};

        printAnyToken(unique_string);
        printAnyToken(weak_string);
        printAnyToken(observer_string);
        printAnyToken(shared_double);
        printAnyToken(unified_double);

        printTokenByType(unique_string);
        printTokenByType(weak_string);
        printTokenByType(observer_string);
        printTokenByType(shared_double);
        printTokenByType(unified_double);
    }

    cout << endl;

    {
        cout << "std smart pointers:" << endl;

        auto    unique_string        = std::make_unique<string>("Hello, World!");
        string* observer_string      = unique_string.get();
        auto    shared_double        = std::make_shared<double>(43.42);
        weak_ptr<double> weak_double = shared_double;

        printAnyToken(unique_string);
        printAnyToken(shared_double);
        printAnyToken(observer_string);
        printAnyToken(weak_double);

        printTokenByType(unique_string);
        printTokenByType(shared_double);
        printTokenByType(observer_string);
        printTokenByType(weak_double);
    }

    return 0;
}
